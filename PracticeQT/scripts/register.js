﻿var Page_url = "";
$(document).ready(function () {
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    // var url = atob(url).split('=');
    var url = url[0].split('=')[1].replace(/%20/g, " ").split(',')[0];
    Page_url = url;
});

function Register() {
    debugger;
    var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    var sUserName = document.getElementById("txtEmail").value;
    var sPassword = $("#txtPassword").val();
    var sConfirmPassword = $("#txtCon_Password").val();
    var sFullName = $("#txtfullname").val();

    var bValid = true;

    if(sFullName == "")
    {
        alert("Enter Full Name.");
        bValid = false;
        return false;
    }

    if (sUserName == "")
    {
        alert("Enter Email.");
        bValid = false;
        return false;
    }
    else if(!re.test(sUserName))
    {
        bValid = false;
        alert("Enter valid email address");
        document.getElementById('txtEmail').focus();
        return false;
    }
    if(sPassword == "")
    {
        bValid = false;
        alert("Enter password.");
        $("#txtPassword").focus();
        return false;
    }
    if(sConfirmPassword == "")
    {
        bValid = false;
        alert("Retype password.");
        $("#txtCon_Password").focus();
        return false;
    }
    else if(sConfirmPassword != sPassword)
    {
        Valid = false;
        alert("Passwords are not same.");
        $("#txtPassword").focus();
        $("#txtPassword").focus();
        return false;
    }
    if($("#agreeTerms").prop("checked") == false)
    {
        bValid = false;
        alert("Agree the term and condition.");
        return false;
    }

    var arrUser = {
        FullName: sFullName,
        Email: sUserName,
        UserName: sUserName,
        Mobile: "",
        Country: "",
        State: "",
        City: "",
        Address: "",
        Zipcode: "",
        Password: sPassword,
        RoleId: "",
    }
    if(bValid == true)
    {
        post("WebServices/UserHandler.asmx/UserRegistration", { arrUser }, function success(data) {
            alert('You have Successfully registered,Please check your E-mail.')
        }, function error(){
            alert(data.errorMessage)
        });
    }
}