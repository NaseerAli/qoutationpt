﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace QoutationTest.BL
{
    public class mongoHelper
    {
      
            public enum mbReturnCode
            {
                SUCCESS = 1,
                CONNECTIONFAILURE = -1,
                SUCCESSNORESULT = -2,
                SUCCESSNOAFFECT = -3,
                DUPLICATEENTRY = -4,
                EXCEPTION = -5,
                INPUTPARAMETEREMPTY = -6,
                CancellationChanged = -7,
                PriceCancleBothChanged = -8,
                PriceChanged = -9,
                SessionExpired = -10,
            }

            public static string currentdb { get; set; }
            public static string currentCollection { get; set; }

            public static string[] arrdbs
            {
                get
                {
                    string db = ConfigurationManager.AppSettings["mongodbs"];
                    // string db = ConfigurationManager.AppSettings["punjani_live"];
                    if (string.IsNullOrEmpty(db))
                        return new string[] { };
                    else
                    {
                        return db.Split(';');
                    }
                }
            }
            public static string[] arrCollection
            {
                get
                {
                    string arrcoll = ConfigurationManager.AppSettings["mongocollects"];
                    if (string.IsNullOrEmpty(arrcoll))
                        return new string[] { };
                    else
                    {
                        return arrcoll.Split(';');
                    }
                }
            }
            public enum Oprator
            {
                Eq = 1,   /*Equal*/
                Not = 2,   /*Not*/
                or = 3,    /*OR*/
                And = 4,   /*And*/
                Gt = 5,    /*Greater*/
            }
            public static IMongoCollection<Object> dtResult
            {
                get
                {
                    try
                    {
                        string sDB = arrdbs.Where(d => d == currentdb).FirstOrDefault();
                        if (string.IsNullOrEmpty(sDB))
                            throw new Exception("Database not fount");
                        var database = mongoClient.GetDatabase(sDB);

                        string sCol = arrCollection.Where(d => d == currentCollection).FirstOrDefault();
                        if (string.IsNullOrEmpty(sCol))
                            throw new Exception("Table not fount");

                        return database.GetCollection<object>(sCol);
                    }
                    catch (Exception ex)
                    {
                        //  ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                        throw new Exception(ex.Message);
                    }
                }
                set { }
            }
            public static MongoClient mongoClient
            {
                get
                {
                    try
                    {
                        string conn = ConfigurationManager.AppSettings["DB_ConnectionString"];
                        //"mongodb+srv://it_trivo2019:ahmed%400729@cluster0-luqnx.azure.mongodb.net/test?retryWrites=true&w=majority";
                        var client = new MongoClient(conn);
                        return client;
                    }
                    catch (Exception ex)
                    {
                        // ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                        throw new Exception("Server not Found");
                    }
                }
            }

            public static bool setCollection
            {
                get
                {
                    try
                    {
                        string sDB = arrdbs.Where(d => d == currentdb).FirstOrDefault();
                        if (string.IsNullOrEmpty(sDB))
                            throw new Exception("Database not fount");
                        var database = mongoClient.GetDatabase(sDB);

                        string sCol = arrCollection.Where(d => d == currentCollection).FirstOrDefault();
                        if (string.IsNullOrEmpty(sCol))
                            throw new Exception("Table not fount");
                        dtResult = database.GetCollection<object>(sCol);
                        return true;
                    }
                    catch (Exception ex)
                    {
                        //  ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                        throw new Exception(ex.Message);
                    }
                }
            }


            public static mbReturnCode _getCollection(out List<object> dtData)
            {
                mbReturnCode retCode = mbReturnCode.EXCEPTION;
                dtData = new List<object>();
                try
                {
                    if (setCollection)
                    {
                        dtData = dtResult.AsQueryable<Object>().ToList();
                        retCode = mbReturnCode.SUCCESS;
                    }
                    else
                        retCode = mbReturnCode.CONNECTIONFAILURE;
                }
                catch (Exception ex)
                {
                    // ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                    throw new Exception(ex.Message);
                }
                finally
                {
                }
                return retCode;
            }

            public static mbReturnCode _find(Object arrData, out List<object> dtData, params object[] arrFileds)
            {
                mbReturnCode retCode = mbReturnCode.SUCCESSNORESULT;
                dtData = new List<object>();
                try
                {
                    if (setCollection)
                    {
                        var json = Newtonsoft.Json.JsonConvert.SerializeObject(arrData);
                        var dictionary = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<object, Object>>(json);
                        var arrFilter = _getFilter(dictionary);
                        if (arrFileds.Length == 0)
                        {
                            var fieldsBuilder = Builders<object>.Projection;
                            var fields = fieldsBuilder.Exclude("_t");
                            //Project(Builders<object>.Projection.Exclude("_t"))
                            dtData = dtResult.Find(arrFilter).ToList();
                        }
                        else
                        {
                            var fieldsBuilder = Builders<object>.Projection;
                            var fields = fieldsBuilder.Include("");
                            var dynamic = arrFileds[0];
                            json = Newtonsoft.Json.JsonConvert.SerializeObject(dynamic);
                            var dfields = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<object, object>>(json);
                            dtData = dtResult.Find(arrFilter).Project<object>(_getParams(dfields)).ToEnumerable().ToList();
                        }
                        if (dtData.Count() != 0)
                            retCode = mbReturnCode.SUCCESS;
                        else
                            retCode = mbReturnCode.SUCCESSNORESULT;
                    }
                    else
                        retCode = mbReturnCode.CONNECTIONFAILURE;
                }
                catch (Exception ex)
                {
                    //  ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                    throw new Exception(ex.Message);
                }
                finally
                {
                }
                return retCode;
            }


            public static mbReturnCode _InsertScalar(object arrData, out string ID)
            {
                mbReturnCode retCode = mbReturnCode.EXCEPTION;
                ID = "";
                try
                {
                    if (setCollection)
                    {
                        var bsonDocument = arrData.ToBsonDocument();
                        dtResult.InsertOne(bsonDocument);
                        retCode = mbReturnCode.SUCCESS;
                    }
                    else
                        retCode = mbReturnCode.CONNECTIONFAILURE;
                }
                catch (Exception ex)
                {
                    //ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                    throw new Exception(ex.Message);
                }
                finally
                {
                }
                return retCode;
            }
            public static mbReturnCode _InsertOne(object arrData)
            {
                mbReturnCode retCode = mbReturnCode.EXCEPTION;
                try
                {
                    if (setCollection)
                    {
                        dtResult.InsertOne(arrData);
                        retCode = mbReturnCode.SUCCESS;
                    }
                    else
                        retCode = mbReturnCode.CONNECTIONFAILURE;
                }
                catch (Exception ex)
                {

                }
                finally
                {
                }
                return retCode;
            }

            public static mbReturnCode _InsertMany(List<object> arrData)
            {
                mbReturnCode retCode = mbReturnCode.EXCEPTION;
                try
                {
                    if (setCollection)
                    {
                        dtResult.InsertMany(arrData);
                        retCode = mbReturnCode.SUCCESS;
                    }
                    else
                        retCode = mbReturnCode.CONNECTIONFAILURE;
                }
                catch (Exception ex)
                {

                }
                finally
                {
                }
                return retCode;
            }
            public static mbReturnCode _update(object arrUpate, object Data)
            {
                mbReturnCode retCode = mbReturnCode.EXCEPTION;
                try
                {
                    if (setCollection)
                    {
                        /*Update By*/
                        var json = Newtonsoft.Json.JsonConvert.SerializeObject(Data);
                        var dictionary = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<Object, Object>>(json);
                        //var doc = _getParams(dictionary);

                        /*Update Data*/
                        json = Newtonsoft.Json.JsonConvert.SerializeObject(arrUpate);
                        var updatedictionary = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<Object, Object>>(json);
                        var Bsondoc = _getParams(updatedictionary);
                        long row = 0;
                        // if (dictionary.Count == 1)
                        {
                            var replacement = _getDoc(dictionary);
                            var filter = _getFilter(updatedictionary);
                            var result = dtResult.UpdateOne(filter, replacement);
                            row = result.MatchedCount;

                        }
                        //if (dictionary.Count > 1)
                        //{
                        //    var replacement = _getDoc(updatedictionary);
                        //    var filter = _getFilter(dictionary);
                        //    var result = dtResult.UpdateMany(filter, replacement);
                        //    row = result.MatchedCount;
                        //}
                        if (row == 0)
                            retCode = mbReturnCode.EXCEPTION;
                        else
                            retCode = mbReturnCode.SUCCESS;
                    }
                    else
                        retCode = mbReturnCode.CONNECTIONFAILURE;
                }
                catch (Exception ex)
                {
                    //ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                    retCode = mbReturnCode.DUPLICATEENTRY;
                }
                finally
                {
                }
                return retCode;
            }

            public static BsonDocument getBsonDocument(object dynamic)
            {

                string json = Newtonsoft.Json.JsonConvert.SerializeObject(dynamic);
                var dfields = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<Object, object>>(json);
                return _getParams(dfields);
            }


            public static BsonDocument _getParams(Dictionary<Object, Object> dataCollection)
            {
                BsonDocument doc = new BsonDocument();
                foreach (var data in dataCollection)
                {
                    doc.Add(data.Key.ToString(), data.Value.ToString());
                }
                return doc;
            }

            public static FilterDefinition<object> _getFilter(Dictionary<Object, Object> dataCollection)
            {
                FilterDefinitionBuilder<object> builder = Builders<object>.Filter;
                FilterDefinition<object> filter = null;
                foreach (var data in dataCollection)
                {
                    if (dataCollection.FirstOrDefault().Key == data.Key)
                        filter = (Builders<object>.Filter.Eq(data.Key.ToString(), data.Value));
                    else
                        filter = filter & (Builders<object>.Filter.Eq(data.Key.ToString(), data.Value));
                }
                return filter;
            }

            public static UpdateDefinition<object> _getDoc(Dictionary<Object, Object> dataCollection)
            {
                var update = new object();
                var updateList = new List<UpdateDefinition<object>>();
                foreach (var data in dataCollection)
                {
                    update = Builders<object>.Update.Set(data.Key.ToString(), data.Value);
                }
                return (UpdateDefinition<object>)update;
            }


            /* for Stuba Hotels */
            public static mbReturnCode _findStubaHotel(string str, out List<object> dtData, params object[] arrFileds)
            {
                mbReturnCode retCode = mbReturnCode.SUCCESSNORESULT;
                dtData = new List<object>();
                try
                {
                    if (setCollection)
                    {
                        var json = "{\"ObjHotelDetails.Region.CityId\":\"" + str + "\"}";
                        // json = JsonConvert.SerializeObject(str);
                        var dictionary = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<object, Object>>(json);
                        var arrFilter = _getFilter(dictionary);
                        if (arrFileds.Length == 0)
                        {
                            var fieldsBuilder = Builders<object>.Projection;
                            var fields = fieldsBuilder.Exclude("_t");
                            //Project(Builders<object>.Projection.Exclude("_t"))
                            dtData = dtResult.Find(arrFilter).ToList();

                            if (dtData.Count() == 0)
                            {
                                var jsonn = "{\"ObjHotelDetails.Region._id\":\"" + str + "\"}";
                                // json = JsonConvert.SerializeObject(str);
                                var dictionaryy = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<object, Object>>(jsonn);
                                var arrFilterr = _getFilter(dictionaryy);
                                if (arrFileds.Length == 0)
                                {
                                    var fieldsBuilderr = Builders<object>.Projection;
                                    var fieldss = fieldsBuilderr.Exclude("_t");
                                    //Project(Builders<object>.Projection.Exclude("_t"))
                                    dtData = dtResult.Find(arrFilterr).ToList();
                                }
                            }
                        }
                        else
                        {
                            var fieldsBuilder = Builders<object>.Projection;
                            var fields = fieldsBuilder.Include("");
                            var dynamic = arrFileds[0];
                            json = Newtonsoft.Json.JsonConvert.SerializeObject(dynamic);
                            var dfields = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<object, object>>(json);
                            dtData = dtResult.Find(arrFilter).Project<object>(_getParams(dfields)).ToEnumerable().ToList();
                        }
                        if (dtData.Count() != 0)
                            retCode = mbReturnCode.SUCCESS;
                        else
                            retCode = mbReturnCode.SUCCESSNORESULT;
                    }
                    else
                        retCode = mbReturnCode.CONNECTIONFAILURE;
                }
                catch (Exception ex)
                {
                    //ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                    throw new Exception(ex.Message);
                }
                finally
                {
                }
                return retCode;
            }

            public static void RemoveDisclimator(string elemName)
            {
                try
                {
                    IMongoDatabase db = mongoClient.GetDatabase(currentdb);
                    var IQData = db.GetCollection<BsonDocument>(currentCollection);
                    var filter = Builders<BsonDocument>.Filter.Empty;
                    var update = Builders<BsonDocument>.Update.Rename("_v", elemName);
                    var unset = Builders<BsonDocument>.Update.Unset("_t");
                    IQData.UpdateMany(filter, update); // to rename _v
                    IQData.UpdateMany(filter, unset);  // to remove _t
                }
                catch (Exception)
                {

                    throw;
                }
            }

            public static mbReturnCode DropCollection(string currentCollection)
            {
                var retCode = mbReturnCode.EXCEPTION;
                try
                {
                    IMongoDatabase db = mongoClient.GetDatabase(currentdb);
                    db.DropCollection(currentCollection);
                    db.CreateCollection(currentCollection);
                    retCode = mbReturnCode.SUCCESS;
                }
                catch (Exception ex)
                {
                    //  ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                    throw new Exception(ex.Message);
                }
                finally { }
                return retCode;
            }
            public static BsonArray _getList<T>(List<T> arrField)
            {
                List<BsonDocument> arrData = new List<BsonDocument>();
                try
                {
                    arrData = arrField.AsEnumerable().Select(d => new BsonDocument(d.ToBsonDocument())).ToList();
                }
                catch (Exception)
                {

                }
                return new BsonArray(arrData);
            }

        }
    }
